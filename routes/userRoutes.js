import express from 'express';
import User from '../models/userModel.js';
import bcrypt from 'bcryptjs';
import { decode, generateToken, isAuth } from '../utils.js';
import expressAsyncHandler from 'express-async-handler';
import { checkEmailExist } from '../controllers/userController.js';

const userRouter = express.Router();

userRouter.post(
  '/signin',
  expressAsyncHandler(async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    if (user) {
      if (bcrypt.compareSync(req.body.password, user.password)) {
        res.send({
          _id: user._id,
          name: user.name,
          email: user.email,
          isAdmin: user.isAdmin,
          token: generateToken(user),
        });
        return;
      } else {
        res.status(401).send({ message: 'Invalid password' });
        return;
      }
    }
    res.status(401).send({ message: 'Email not found.' });
  })
);

userRouter.post(
  '/signup',
  expressAsyncHandler(async (req, res) => {
    const emailExist = await checkEmailExist(req.body);
    if (emailExist) {
      res.send({
        message: 'Email is already registered to another user.',
      });
    } else {
      if (req.body.name.length < 3) {
        res.send({ message: 'Name must contain at least 3 characters.' });
      } else if (req.body.password.length < 4) {
        res.send({ message: 'Password must contain at least 4 characters.' });
      } else {
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          password: bcrypt.hashSync(req.body.password),
        });
        const user = await newUser.save();
        res.send({
          _id: user._id,
          name: user.name,
          email: user.email,
          isAdmin: user.isAdmin,
          token: generateToken(user),
        });
      }
    }
  })
);

userRouter.put(
  '/profile',
  isAuth,
  expressAsyncHandler(async (req, res, next) => {
    const user = await User.findById(req.user._id);

    if (user) {
      user.name = req.body.name || user.name;
      user.email = req.body.email || user.email;
      if (req.body.password) {
        user.password = bcrypt.hashSync(req.body.password, 8);
      }

      const updatedUser = await user.save();
      res.send({
        _id: updatedUser._id,
        name: updatedUser.name,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        token: generateToken(updatedUser),
      });
    } else {
      res.status(404).send({ message: 'User Not Found' });
    }
  })
);

userRouter.patch(
  '/profile/updateName',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id);

    if (req.body.name.length >= 3) {
      user.name = req.body.name;
      const updatedUser = await user.save();
      res.send({
        _id: updatedUser._id,
        name: updatedUser.name,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        token: generateToken(updatedUser),
      });
    } else {
      res.send({
        message: 'Name must contain at least 3 characters.',
        _id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        token: generateToken(user),
      });
    }
  })
);

userRouter.patch(
  '/profile/updateEmail',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id);

    if (user) {
      const emailExist = await checkEmailExist(req.body);

      // if email is already registered
      if (emailExist !== false) {
        // checks if the inputted email is the same as his/her current registered email
        if (user.email === req.body.email) {
          res.send({
            message: 'Email not updated as there was no change.',
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            token: generateToken(user),
          });
        } else {
          res.send({
            message: 'Email is already registered to another user. Please use a different email.',
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            token: generateToken(user),
          });
        }
      } else {
        user.email = req.body.email;
        const updatedUser = await user.save();
        res.send({
          _id: updatedUser._id,
          name: updatedUser.name,
          email: updatedUser.email,
          isAdmin: updatedUser.isAdmin,
          token: generateToken(updatedUser),
        });
      }
    } else {
      res.status(404).send({
        message: 'User Not Found',
        _id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        token: generateToken(user),
      });
    }
  })
);

userRouter.post(
  '/profile/verifyPassword',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id);
    if (bcrypt.compareSync(req.body.password, user.password)) {
      res.send(true);
      // console.log('Correct Password!');
      return;
    }
    res.status(401).send({ message: 'Invalid password' });
  })
);

userRouter.patch(
  '/profile/updatePassword',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id);
    // console.log(req.body.newPassword.length);
    if (req.body.newPassword.length < 4) {
      res.send({
        message: 'Password not updated as it must contain at least 4 characters.',
        _id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        token: generateToken(user),
      });
    } else {
      user.password = bcrypt.hashSync(req.body.newPassword);
      const updatedUser = await user.save();
      // console.log(updatedUser);
      res.send({
        _id: updatedUser._id,
        name: updatedUser.name,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        token: generateToken(updatedUser),
      });
    }
  })
);

export default userRouter;

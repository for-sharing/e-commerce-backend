import express from 'express';
import Product from '../models/productModel.js';
import mockData from '../mockData.js';
import User from '../models/userModel.js';

const seedRouter = express.Router();

seedRouter.get('/', async (req, res) => {
  await Product.remove({});
  const createdProducts = await Product.insertMany(mockData.products);
  await User.remove({});
  const createdUsers = await User.insertMany(mockData.users);
  res.send({ createdProducts, createdUsers });
});

export default seedRouter;

import bcrypt from 'bcryptjs';

const mockData = {
  users: [
    {
      name: 'Admin',
      email: 'admin@email.com',
      password: bcrypt.hashSync('admin123'),
      isAdmin: true,
    },
    {
      name: 'User',
      email: 'user@email.com',
      password: bcrypt.hashSync('guest1234'),
      isAdmin: false,
    },
  ],
  products: [
    {
      name: 'LeBron Jersey White',
      slug: 'lebron-jersey-white',
      category: 'Jersey',
      image: '/images/lebron_upper_jersey_white.webp',
      price: 120,
      countInStock: 10,
      brand: 'Nike',
      rating: 4.5,
      numReviews: 10,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Curry Jersey Dark Blue',
      slug: 'curry-jersey-dark-blue',
      category: 'Jersey',
      image: '/images/curry_upper_jersey_3.webp',
      price: 120,
      countInStock: 21,
      brand: 'Jordan',
      rating: 4,
      numReviews: 25,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'KD15 ',
      slug: 'durant-shoes-1',
      category: 'Shoes',
      image: '/images/durant_shoes_1.webp',
      price: 75,
      countInStock: 8,
      brand: 'Nike',
      rating: 2.5,
      numReviews: 6,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'LeBron Jersey Purple',
      slug: 'lebron-jersey-purple',
      category: 'Jersey',
      image: '/images/lebron_upper_jersey_purple.webp',
      price: 120,
      countInStock: 15,
      brand: 'Jordan',
      rating: 4.5,
      numReviews: 41,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'AD Jersey White',
      slug: 'ad-jersey-white',
      category: 'Jersey',
      image: '/images/ad_upper_jersey_white.webp',
      price: 65,
      countInStock: 5,
      brand: 'Nike',
      rating: 3,
      numReviews: 10,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Durant Jersey City Edition',
      slug: 'durant-jersey-city-edition',
      category: 'Jersey',
      image: '/images/durant_upper_jersey_3.webp',
      price: 200,
      countInStock: 50,
      brand: 'Nike',
      rating: 3,
      numReviews: 10,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'AD Jersey Purple',
      slug: 'ad-jersey-purple',
      category: 'Jersey',
      image: '/images/ad_upper_jersey_purple.webp',
      price: 120,
      countInStock: 41,
      brand: 'Jordan',
      rating: 2,
      numReviews: 10,
      description: 'high quality product',
      isActive: true,
    },

    {
      name: 'LeBron Jersey Gold',
      slug: 'lebron-jersey-gold',
      category: 'Jersey',
      image: '/images/lebron_upper_jersey_gold.webp',
      price: 120,
      countInStock: 20,
      brand: 'Nike',
      rating: 4.8,
      numReviews: 34,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Curry Jersey White',
      slug: 'curry-jersey-white',
      category: 'Jersey',
      image: '/images/curry_upper_jersey_2.webp',
      price: 120,
      countInStock: 25,
      brand: 'Nike',
      rating: 3,
      numReviews: 31,
      description: 'high quality product',
      isActive: true,
    },

    {
      name: 'KD15 ASW',
      slug: 'durant-shoes-2',
      category: 'Shoes',
      image: '/images/durant_shoes_2.webp',
      price: 70,
      countInStock: 5,
      brand: 'Nike',
      rating: 1,
      numReviews: 7,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Curry Jersey Blue',
      slug: 'curry-jersey-blue',
      category: 'Jersey',
      image: '/images/curry_upper_jersey_1.webp',
      price: 120,
      countInStock: 14,
      brand: 'Nike',
      rating: 1.5,
      numReviews: 40,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'KD15 Trey 5 IX',
      slug: 'durant-shoes-3',
      category: 'Shoes',
      image: '/images/durant_shoes_3.webp',
      price: 75,
      countInStock: 7,
      brand: 'Nike',
      rating: 3,
      numReviews: 11,
      description: 'high quality product',
      isActive: true,
    },

    {
      name: 'Durant Jersey White',
      slug: 'durant-jersey-white',
      category: 'Jersey',
      image: '/images/durant_upper_jersey_2.webp',
      price: 120,
      countInStock: 26,
      brand: 'Nike',
      rating: 4,
      numReviews: 17,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Giannis Jersey Blue',
      slug: 'giannis-jersey-2',
      category: 'Jersey',
      image: '/images/giannis_upper_jersey_2.webp',
      price: 120,
      countInStock: 45,
      brand: 'Nike',
      rating: 4.5,
      numReviews: 12,
      description: 'high quality product',
      isActive: true,
    },

    {
      name: 'Kyrie Immortality 2',
      slug: 'kyrie-shoes-1',
      category: 'Shoes',
      image: '/images/kyrie_shoes_1.webp',
      price: 70,
      countInStock: 4,
      brand: 'Nike',
      rating: 4.4,
      numReviews: 8,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Durant Jersey Black',
      slug: 'durant-jersey-black',
      category: 'Jersey',
      image: '/images/durant_upper_jersey_1.webp',
      price: 120,
      countInStock: 35,
      brand: 'Nike',
      rating: 4,
      numReviews: 23,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Kyrie Flytrap 6',
      slug: 'kyrie-shoes-2',
      category: 'Shoes',
      image: '/images/kyrie_shoes_2.webp',
      price: 75,
      countInStock: 5,
      brand: 'Nike',
      rating: 3.3,
      numReviews: 10,
      description: 'high quality product',
      isActive: true,
    },

    {
      name: 'Luka Jersey White',
      slug: 'luka-jersey-2',
      category: 'Jersey',
      image: '/images/luka_upper_jersey_2.webp',
      price: 120,
      countInStock: 80,
      brand: 'Nike',
      rating: 3,
      numReviews: 9,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Giannis Jersey Purple',
      slug: 'giannis-jersey-3',
      category: 'Jersey',
      image: '/images/giannis_upper_jersey_3.webp',
      price: 120,
      countInStock: 65,
      brand: 'Nike',
      rating: 4,
      numReviews: 13,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Kyrie Jersey White',
      slug: 'kyrie-jersey-1',
      category: 'Jersey',
      image: '/images/kyrie_upper_jersey_1.webp',
      price: 120,
      countInStock: 35,
      brand: 'Nike',
      rating: 2.5,
      numReviews: 31,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Giannis Jersey Green',
      slug: 'giannis-jersey-1',
      category: 'Jersey',
      image: '/images/giannis_upper_jersey_1.webp',
      price: 120,
      countInStock: 77,
      brand: 'Nike',
      rating: 4,
      numReviews: 21,
      description: 'high quality product',
      isActive: true,
    },

    {
      name: 'Lebron XX SE',
      slug: 'lebron-shoes-3',
      category: 'Shoes',
      image: '/images/lebron_shoes_3.jfif',
      price: 95,
      countInStock: 11,
      brand: 'Nike',
      rating: 4.9,
      numReviews: 13,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Luka Jersey Dark Blue',
      slug: 'luka-jersey-3',
      category: 'Jersey',
      image: '/images/luka_upper_jersey_3.webp',
      price: 120,
      countInStock: 46,
      brand: 'Nike',
      rating: 3,
      numReviews: 13,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Lebron Witness 7',
      slug: 'lebron-shoes-1',
      category: 'Shoes',
      image: '/images/lebron_shoes_1.webp',
      price: 95,
      countInStock: 7,
      brand: 'Nike',
      rating: 4.5,
      numReviews: 18,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Nike Lebron 9 Low',
      slug: 'lebron-shoes-2',
      category: 'Shoes',
      image: '/images/lebron_shoes_2.jfif',
      price: 150,
      countInStock: 4,
      brand: 'Nike',
      rating: 1,
      numReviews: 13,
      description: 'high quality product',
      isActive: true,
    },
    {
      name: 'Luka Jersey Blue',
      slug: 'luka-jersey-1',
      category: 'Jersey',
      image: '/images/luka_upper_jersey_1.webp',
      price: 120,
      countInStock: 65,
      brand: 'Nike',
      rating: 0.5,
      numReviews: 10,
      description: 'high quality product',
      isActive: true,
    },
  ],
};

export default mockData;

import User from '../models/userModel.js';

export const checkEmailExist = async (reqBody) => {
  //   console.log(reqBody.email);
  const result = await User.findOne({ email: reqBody.email });

  if (result) {
    return result;
  } else {
    return false;
  }
};

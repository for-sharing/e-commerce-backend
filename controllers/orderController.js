import Order from '../models/orderModel.js';
import { subtractProductStocks } from './productController.js';

export const checkout = (user, reqBody) => {
  const newOrder = new Order({
    orderItems: reqBody.orderItems.map((x) => ({
      ...x,
      product: x._id,
    })),
    shippingAddress: reqBody.shippingAddress,
    paymentMethod: reqBody.paymentMethod,
    itemsPrice: reqBody.itemsPrice,
    shippingPrice: reqBody.shippingPrice,
    taxPrice: reqBody.taxPrice,
    totalPrice: reqBody.totalPrice,
    user: user._id,
  });

  // console.log(newOrder.shippingAddress);
  newOrder.orderItems.map((orderProduct) => {
    subtractProductStocks(orderProduct._id, orderProduct.quantity);
  });

  //   console.log(reqBody.orderItems);
  //   console.log(newOrder.orderItems);

  return newOrder.save().then((order, error) => {
    if (error) {
      return { message: 'Unexpected error occurred, please redo your order.' };
    } else {
      return { message: 'Order successful', order };
    }
  });
};

import Product from '../models/productModel.js';

export const subtractProductStocks = async (productId, subtractedStocks) => {
  const currentProduct = await Product.findById(productId).then(
    (product, error) => {
      if (error) {
        return { message: 'Product Not Found' };
      } else {
        return product;
      }
    }
  );
  //   console.log(currentProduct);
  //   console.log(subtractedStocks);
  //   console.log(currentProduct.countInStock);
  currentProduct.countInStock = currentProduct.countInStock - subtractedStocks;
  //   console.log(currentProduct.countInStock);
  if (currentProduct.countInStock === 0) {
    autoArchiveProduct(productId);
  }

  return currentProduct.save();
};

const autoArchiveProduct = async (productId) => {
  let updateActiveField = {
    isActive: false,
  };

  return Product.findByIdAndUpdate(productId, updateActiveField).then(
    (product, error) => {
      if (error) {
        return { message: 'Product Not Found' };
      } else {
        return product;
      }
    }
  );
};
